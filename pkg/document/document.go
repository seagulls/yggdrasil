package document

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/seagulls/yggdrasil/pkg/action"
	"gitlab.com/seagulls/yggdrasil/pkg/node"
	"gitlab.com/seagulls/yggdrasil/pkg/path"
	"gitlab.com/seagulls/yggdrasil/pkg/store"
	"gitlab.com/seagulls/yggdrasil/pkg/version"
)

// Document is a container which manages the tree data structure and handles
// the recording of changes applied to the data.
type Document struct {
	store store.Store
	mut   sync.RWMutex

	ID      string
	Version version.Semantic
	Root    node.Node
}

func New(guid string, root node.Node, s store.Store) *Document {
	return &Document{
		store: s,

		ID:      guid,
		Version: version.Semantic{},
		Root:    root,
	}
}

// Apply takes an action and a location as it's arguments and applies the action
// at the specified location.  If the action is successful then the action is recorded
// in the store, rolling back the action if the store operation fails to ensure
// store/data parity.
func (d *Document) Apply(a action.Action, at path.Path) error {
	d.mut.Lock()
	defer d.mut.Unlock()

	rootKey, at := at.Shift()
	if !rootKey.Equals(d.Root.Key()) {
		return fmt.Errorf(`incorrect root node %#v`, rootKey)
	}

	n, ok := d.Root.Child(at)
	if !ok {
		return node.ErrChildNotExists{
			P: at,
		}
	}

	err := a.Apply(n)
	if err != nil {
		return err
	}

	change := &store.Change{
		DocumentID: d.ID,
		Action:     a,
		At:         at,
		Timestamp:  time.Now().In(time.UTC),
		Version: version.Semantic{
			Major: d.Version.Major,
			Minor: d.Version.Minor,
			Patch: d.Version.Patch + 1,
		},
	}

	err = d.store.Write(change)
	if err != nil {
		// failed to write the change, so we must rollback the change
		a.Rollback()
		return err
	}

	// update succeeded so update the document version to match the change
	d.Version = change.Version
	return nil
}

// Attr fetches the value at a given location.  In this method the path passed in
// is assumed to have the attribute name as the last element.  As with map access,
// a bool true is returned with the attribute if it exists, false if it does not.
func (d *Document) Attr(at path.Path) (interface{}, bool) {
	// ensure this path is for this document by checking root element name
	rootKey, at := at.Shift()
	if rootKey != d.Root.Key() {
		return nil, false
	}

	// pop the attr name assert name marker
	marker, at := at.Pop()
	key, ok := marker.(path.Name)
	if !ok {
		return nil, false
	}

	// fetch child at
	n, ok := d.Root.Child(at)
	if !ok {
		return nil, false
	}

	return n.Attr(string(key))
}
