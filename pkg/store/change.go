package store

import (
	"time"

	"gitlab.com/seagulls/yggdrasil/pkg/action"
	"gitlab.com/seagulls/yggdrasil/pkg/path"
	"gitlab.com/seagulls/yggdrasil/pkg/version"
)

type Change struct {
	DocumentID string
	Action     action.Action
	At         path.Path
	Timestamp  time.Time
	Version    version.Semantic
}
