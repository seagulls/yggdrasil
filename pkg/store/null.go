package store

// Null is a store which does nothing, primarily used for testing.
type Null struct{}

func (n *Null) Write(_ *Change) error {
	return nil
}

func (n *Null) Read(_ string) ([]*Change, error) {
	return []*Change{}, nil
}
