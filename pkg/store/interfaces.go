package store

// Store allows the reading and writing of the actions which represent
// state changes within a Document.
type Store interface {
	// Write persists a change in the store
	Write(*Change) error
	// Read fetches all changes for a given key
	Read(string) ([]*Change, error)
}

type Marshaller interface {
	Marshal(*Change) ([]byte, error)
	Unmarshal([]byte) (*Change, error)
}
