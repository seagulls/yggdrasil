package store

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"sync"
)

type fileStore struct {
	// path is the location of the directory in which to store document files
	// and can be absolute or relative.
	path string
	// m is the Change Marshaller to use
	m   Marshaller
	mut sync.RWMutex
}

func NewFile(path string, m Marshaller) (Store, error) {
	if err := os.MkdirAll(path, os.ModePerm); err != nil {
		return nil, err
	}

	return &fileStore{
		path: path,
		m:    m,
		mut:  sync.RWMutex{},
	}, nil
}

func (f *fileStore) Write(c *Change) error {
	raw, err := f.m.Marshal(c)
	if err != nil {
		return err
	}

	f.mut.Lock()
	defer f.mut.Unlock()

	path := filepath.Join(f.path, c.DocumentID)
	fp, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer fp.Close()
	if _, err := fmt.Fprintln(fp, string(raw)); err != nil {
		return err
	}

	return nil
}

func (f *fileStore) Read(guid string) ([]*Change, error) {
	f.mut.RLock()
	defer f.mut.RUnlock()

	p := filepath.Join(f.path, guid)
	fp, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer fp.Close()

	changes := make([]*Change, 0, 100)

	scanner := bufio.NewScanner(fp)
	for scanner.Scan() {
		c, err := f.m.Unmarshal(scanner.Bytes())
		if err != nil {
			return nil, err
		}

		changes = append(changes, c)
	}

	return changes, nil
}
