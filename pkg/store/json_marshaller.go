package store

import (
	"encoding/json"
	"time"

	"gitlab.com/seagulls/yggdrasil/pkg/action"
	"gitlab.com/seagulls/yggdrasil/pkg/path"
	"gitlab.com/seagulls/yggdrasil/pkg/version"
)

// jsonChange is a JSON encodable container for the Action interface
type jsonChange struct {
	DocumentID string           `json:"id"`
	Action     json.RawMessage  `json:"action"`
	At         string           `json:"at"`
	Timestamp  time.Time        `json:"timestamp"`
	Version    version.Semantic `json:"version"`
}

// jsonMarshaller provides a two-way mapping between types implementing
// the Action interface and string representations thereof
type jsonMarshaller struct {
	actionMarshaller action.Marshaller
}

func NewJSONMarshaller(a action.Marshaller) Marshaller {
	return &jsonMarshaller{
		actionMarshaller: a,
	}
}

func (j jsonMarshaller) Marshal(c *Change) ([]byte, error) {
	// encode action
	act, err := j.actionMarshaller.Marshal(c.Action)
	if err != nil {
		return nil, err
	}

	source := jsonChange{
		DocumentID: c.DocumentID,
		Action:     act,
		At:         c.At.String(),
		Timestamp:  c.Timestamp.In(time.UTC),
		Version:    c.Version,
	}

	return json.Marshal(source)

}

func (j jsonMarshaller) Unmarshal(raw []byte) (*Change, error) {
	var decoded jsonChange

	err := json.Unmarshal(raw, &decoded)
	if err != nil {
		return nil, err
	}

	// decode path
	at, err := path.New(decoded.At)
	if err != nil {
		return nil, err
	}

	// decode action
	act, err := j.actionMarshaller.Unmarshal(decoded.Action)
	if err != nil {
		return nil, err
	}

	return &Change{
		DocumentID: decoded.DocumentID,
		Action:     act,
		At:         at,
		Timestamp:  decoded.Timestamp,
		Version:    decoded.Version,
	}, nil
}
