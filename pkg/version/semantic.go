package version

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	Patch = 1 << iota
	Minor
	Major
)

// Semantic represents a semantic version number
type Semantic struct {
	Major int
	Minor int
	Patch int
}

func (v *Semantic) Marshal() ([]byte, error) {
	return []byte(fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)), nil
}

func (v *Semantic) Unmarshal(raw []byte) error {
	parts := strings.Split(string(raw), `.`)
	if len(parts) != 3 {
		return InvalidVersionString{S: string(raw)}
	}

	n, err := strconv.ParseInt(parts[0], 10, 64)
	if err != nil {
		return InvalidVersionString{S: string(raw)}
	}
	v.Major = int(n)

	n, err = strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return InvalidVersionString{S: string(raw)}
	}
	v.Minor = int(n)

	n, err = strconv.ParseInt(parts[2], 10, 64)
	if err != nil {
		return InvalidVersionString{S: string(raw)}
	}
	v.Patch = int(n)

	return nil
}

// Increment takes a set of flags and increments the appropriate semantic version levels.
// When incrementing a version level all levels below are reset to 0.  Multiple flags may
// be joined (OR) if multiple levels of the semantic version need to be incremented.
//   version.Major = 1 << 2 = 4
//   version.Minor = 1 << 1 = 2
//   version.Patch = 1 << 0 = 1
// So if semantic version v == 3.6.7, calling v2 := v.Increment(version.Major|version.Patch)
// will result in v2 == 4.0.1
func (v Semantic) Increment(flags int) Semantic {
	out := Semantic{
		Major: v.Major,
		Minor: v.Minor,
		Patch: v.Patch,
	}

	if (flags & Major) > 0 {
		out.Major = out.Major + 1
		out.Minor = 0
		out.Patch = 0
	}

	if (flags & Minor) > 0 {
		out.Minor = out.Minor + 1
		out.Patch = 0
	}

	if (flags & Patch) > 0 {
		out.Patch = out.Patch + 1
	}

	return out
}

// CompareTo will return an integer value comparing this version to the
// provided target version.  The return value will be 0 if they are identical,
// negative if this version is less than the target and positive if this
// version is greater than the target.
func (v Semantic) CompareTo(target Semantic) int {
	comp := v.Major - target.Major
	if comp != 0 {
		return comp
	}

	comp = v.Minor - target.Minor
	if comp != 0 {
		return comp
	}

	return v.Patch - target.Patch
}

// Equals returns true if this version is equal to the target version.
func (v Semantic) Equals(target Semantic) bool {
	return v.CompareTo(target) == 0
}

// GreaterThan returns true if this version is greater than the target.
func (v Semantic) GreaterThan(target Semantic) bool {
	return v.CompareTo(target) > 0
}

// LessThan returns true if this version is less than the target.
func (v Semantic) LessThan(target Semantic) bool {
	return v.CompareTo(target) < 0
}
