package version

import (
	"fmt"
)

type InvalidVersionString struct {
	S string
}

func (err InvalidVersionString) Error() string {
	return fmt.Sprintf("invalid version string %q", err.S)
}
