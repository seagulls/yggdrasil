package version_test

import (
	"testing"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/yggdrasil/pkg/version"
)

func TestSemantic_Marshal(t *testing.T) {
	c := qt.New(t)
	expected := []byte("1.2.345")
	source := version.Semantic{
		Major: 1,
		Minor: 2,
		Patch: 345,
	}

	actual, err := source.Marshal()
	c.Assert(err, qt.IsNil)
	c.Assert(expected, qt.ContentEquals, actual)
}

func TestSemantic_Unmarshal(t *testing.T) {
	c := qt.New(t)
	expected := version.Semantic{
		Major: 1,
		Minor: 2,
		Patch: 345,
	}
	source := "1.2.345"

	var actual version.Semantic
	err := actual.Unmarshal([]byte(source))
	c.Assert(err, qt.IsNil)
	c.Assert(expected, qt.Equals, actual)
}

func TestSemantic_CompareTo(t *testing.T) {
	c := qt.New(t)
	A := version.Semantic{
		Major: 1,
		Minor: 2,
		Patch: 345,
	}

	B := version.Semantic{
		Major: 2,
		Minor: 3,
		Patch: 456,
	}

	C := version.Semantic{
		Major: 1,
		Minor: 3,
		Patch: 456,
	}

	D := version.Semantic{
		Major: 1,
		Minor: 2,
		Patch: 456,
	}

	c.Assert(0, qt.Equals, A.CompareTo(A))
	c.Assert(-1, qt.Equals, A.CompareTo(B))
	c.Assert(-1, qt.Equals, A.CompareTo(C))
	c.Assert(-111, qt.Equals, A.CompareTo(D))
	c.Assert(1, qt.Equals, B.CompareTo(A))
	c.Assert(1, qt.Equals, C.CompareTo(A))
	c.Assert(111, qt.Equals, D.CompareTo(A))
}
