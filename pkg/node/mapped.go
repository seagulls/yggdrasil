package node

import (
	"fmt"

	"gitlab.com/seagulls/yggdrasil/pkg/path"
)

type Mapped map[path.Name]Node

func (m Mapped) Get(marker path.Marker) (Node, bool) {
	key, ok := marker.(path.Name)
	if !ok {
		return nil, false
	}

	v, ok := m[key]
	return v, ok
}

func (m Mapped) Set(n Node) error {
	if n.Key() == path.Marker(nil) {
		return fmt.Errorf(`mapped node missing key`)
	}

	key, ok := n.Key().(path.Name)
	if !ok {
		return fmt.Errorf(`expecting Name key, got %#v`, n.Key())
	}

	if _, ok = m[key]; ok {
		return ErrChildExists{P: path.Path{key}}
	}

	m[key] = n
	return nil
}

func (m Mapped) Unset(marker path.Marker) {
	key, ok := marker.(path.Name)
	if !ok {
		// if the key is invalid then there cannot be an element to remove
		return
	}

	if _, ok = m[key]; ok {
		delete(m, key)
	}
}
