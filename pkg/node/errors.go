package node

import (
	"fmt"

	"gitlab.com/seagulls/yggdrasil/pkg/path"
)

type ErrChildHasParent struct {
	P path.Path
}

func (err ErrChildHasParent) Error() string {
	return fmt.Sprintf("child already has parent: %s", err.P)
}

type ErrChildExists struct {
	P path.Path
}

func (err ErrChildExists) Error() string {
	return fmt.Sprintf("child already exists: %s", err.P)
}

type ErrChildNotExists struct {
	P path.Path
}

func (err ErrChildNotExists) Error() string {
	return fmt.Sprintf("child does not exist: %s", err.P)
}
