package node

import (
	"gitlab.com/seagulls/yggdrasil/pkg/path"
)

type Node interface {
	Key() path.Marker
	Parent() Node
	setParent(Node)
	Path() path.Path
	Root() Node
	Attr(key string) (interface{}, bool)
	HasAttr(key string) bool
	RemoveAttr(key string)
	SetAttr(key string, value interface{})
	Child(path.Path) (Node, bool)
	HasChild(path.Path) bool
	RemoveChild(key path.Marker)
	SetChild(Node) error
}

type Collection interface {
	Get(path.Marker) (Node, bool)
	Set(Node) error
	Unset(path.Marker)
}
