package node

import (
	"gitlab.com/seagulls/yggdrasil/pkg/path"
)

type node struct {
	key        path.Marker
	parent     Node
	attributes map[string]interface{}
	children   Collection
}

func New(key path.Marker, c Collection) Node {
	return &node{
		key:        key,
		parent:     nil,
		attributes: make(map[string]interface{}),
		children:   c,
	}
}

func (n *node) Key() path.Marker {
	return n.key
}

func (n *node) Parent() Node {
	return n.parent
}

func (n *node) setParent(parent Node) {
	n.parent = parent
}

func (n *node) Path() path.Path {
	if n.parent == nil {
		return path.Path{n.key}
	}

	return append(n.parent.Path(), n.key)
}

func (n *node) Root() Node {
	if n.parent == nil {
		return n
	}

	return n.parent.Root()
}

func (n *node) Attr(key string) (interface{}, bool) {
	v, ok := n.attributes[key]
	return v, ok
}

func (n *node) HasAttr(key string) bool {
	_, ok := n.attributes[key]
	return ok
}

func (n *node) RemoveAttr(key string) {
	delete(n.attributes, key)
}

func (n *node) SetAttr(key string, value interface{}) {
	n.attributes[key] = value
}

func (n *node) Child(p path.Path) (Node, bool) {
	// if there is no more path to follow then this is the requested child
	if p == nil || p.Len() == 0 {
		return n, true
	}

	// pop the next child key off the path
	key, childPath := p.Shift()

	// check the child exists
	c, ok := n.children.Get(key)
	if !ok {
		return nil, false
	}

	// pass the child request downstream
	return c.Child(childPath)
}

func (n *node) HasChild(p path.Path) bool {
	_, ok := n.Child(p)
	return ok
}

func (n *node) RemoveChild(key path.Marker) {
	c, ok := n.children.Get(key)
	if ok {
		c.setParent(nil)
		n.children.Unset(key)
	}
}

func (n *node) SetChild(child Node) error {
	if child.Parent() != nil {
		return ErrChildHasParent{
			P: child.Parent().Path(),
		}
	}

	err := n.children.Set(child)
	if err != nil {
		return err
	}
	child.setParent(n)

	return nil
}
