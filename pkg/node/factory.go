package node

import (
	"gitlab.com/seagulls/yggdrasil/pkg/path"
)

func NewMappedNode(key string) Node {
	return New(path.Name(key), make(Mapped))
}
