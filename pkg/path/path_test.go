package path_test

import (
	"fmt"
	"testing"

	qt "github.com/frankban/quicktest"

	path2 "gitlab.com/seagulls/yggdrasil/pkg/path"
)

func TestNewPath(t *testing.T) {
	c := qt.New(t)
	expected := path2.Path{
		path2.Name(`foo`),
		path2.Name(`bar`),
		path2.Index(8),
		path2.Name(`value`),
	}

	raw := "foo.bar[8].value"
	actual, err := path2.New(raw)

	c.Assert(err, qt.IsNil)
	c.Assert(expected.Equals(actual), qt.IsTrue)
}

func TestPath_Len(t *testing.T) {
	c := qt.New(t)
	p := path2.Path{
		path2.Name(`foo`),
		path2.Name(`bar`),
		path2.Index(8),
		path2.Name(`value`),
	}

	expected := 4
	actual := p.Len()

	c.Assert(expected, qt.Equals, actual)
}

func TestPath_Shift(t *testing.T) {
	c := qt.New(t)
	var m path2.Marker
	p := path2.Path{
		path2.Name(`foo`),
		path2.Name(`bar`),
		path2.Index(8),
		path2.Name(`value`),
	}

	testCases := []struct {
		Marker path2.Marker
		Path   path2.Path
	}{
		{
			Marker: path2.Name(`foo`),
			Path: path2.Path{
				path2.Name(`bar`),
				path2.Index(8),
				path2.Name(`value`),
			},
		},
		{
			Marker: path2.Name(`bar`),
			Path: path2.Path{
				path2.Index(8),
				path2.Name(`value`),
			},
		},
		{
			Marker: path2.Index(8),
			Path: path2.Path{
				path2.Name(`value`),
			},
		},
		{
			Marker: path2.Name(`value`),
			Path:   path2.Path{},
		},
		{
			Marker: nil,
			Path:   path2.Path{},
		},
	}

	for idx, tc := range testCases {
		t.Run(fmt.Sprintf("case %d", idx), func(t *testing.T) {
			m, p = p.Shift()

			if m == nil {
				c.Assert(tc.Marker, qt.IsNil)
			} else {
				c.Assert(tc.Marker.Equals(m), qt.IsTrue)
			}
			c.Assert(tc.Path.Equals(p), qt.IsTrue)
		})
	}
}

func TestPath_Encode(t *testing.T) {
	c := qt.New(t)
	p := path2.Path{
		path2.Name(`foo`),
		path2.Name(`bar`),
		path2.Index(8),
		path2.Name(`value`),
	}

	expected := []byte("foo.bar[8].value")
	actual := p.Marshal()

	c.Assert(expected, qt.ContentEquals, actual)
}
