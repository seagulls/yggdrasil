package path

import (
	"fmt"
)

type PathNotRelated struct {
	P Path
	C Path
}

func (err PathNotRelated) Error() string {
	return fmt.Sprintf("path %v not a parent of %v", err.P, err.C)
}

type PathNodeInvalid struct {
	P Path
	N string
}

func (err PathNodeInvalid) Error() string {
	return fmt.Sprintf("node %v of path %v is invalid", err.N, err.P)
}
