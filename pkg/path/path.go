package path

import (
	"fmt"
	"strconv"
	"strings"
)

// Path contains the sequential names of Nodes leading to a destination.  This
// may be a target Node or an attribute of a Node depending on context.
type Path []Marker

func New(encoded string) (Path, error) {
	p := Path{}
	err := p.Unmarshal([]byte(encoded))
	return p, err
}

// MustParse creates a new Path and will panic if an error is returned.
func MustParse(encoded string) Path {
	p, err := New(encoded)
	if err != nil {
		panic(err)
	}

	return p
}

// Equals returns true if the target is exactly the same as p
func (p Path) Equals(target Path) bool {
	if p.Len() != target.Len() {
		return false
	}

	for i := range p {
		if p[i] != target[i] {
			return false
		}
	}

	return true
}

// Pop removes the last element of p and returns it along
// with the remaining path.
func (p Path) Pop() (Marker, Path) {
	if len(p) == 0 {
		return nil, nil
	}

	last := len(p) - 1
	return p[last], p[:last]
}

// Shift removes the first element of p and returns it along
// with the remaining path.
func (p Path) Shift() (Marker, Path) {
	if len(p) == 0 {
		return nil, nil
	}

	if len(p) == 1 {
		return p[0], Path{}
	}

	return p[0], p[1:]
}

func (p Path) Len() int {
	return len(p)
}

func (p *Path) String() string {
	return string(p.Marshal())
}

func (p *Path) Marshal() []byte {
	parts := make([]string, 0, len(*p))

	for _, v := range *p {
		switch m := v.(type) {
		case Name:
			// append to the list of path parts
			parts = append(parts, string(m))
		case Index:
			// add to the last path part as an array index
			parts[len(parts)-1] = fmt.Sprintf(`%s[%d]`, parts[len(parts)-1], int(m))
		}
	}

	return []byte(strings.Join(parts, `.`))
}

func (p *Path) Unmarshal(b []byte) error {
	parts := strings.Split(string(b), `.`)

	np := make([]Marker, 0, len(parts))
	for _, key := range parts {
		subs := strings.Split(key, `[`)
		np = append(np, Name(subs[0]))

		if len(subs) > 1 {
			for _, v := range subs[1:] {
				// convert v (trimmed of trailing `]`) to an int
				idx, err := strconv.ParseInt(v[:len(v)-1], 10, 0)
				if err != nil {
					return err
				}
				np = append(np, Index(idx))
			}
		}
	}

	// copy over the slice
	*p = np

	return nil
}

// ChildOf returns true if path is a child of p
func (p Path) ChildOf(path Path) bool {
	return path.ParentOf(p)
}

// ParentOf returns true if path is a parent of p
func (p Path) ParentOf(path Path) bool {
	if p.Len() > path.Len() {
		return false
	}

	for k, v := range p {
		if v != path[k] {
			return false
		}
	}

	return true
}

// RelativeTo returns the path of p relative to path as long as path
// is a parent of p
func (p Path) RelativeTo(path Path) (Path, error) {
	if !path.ParentOf(p) {
		return nil, PathNotRelated{P: path, C: p}
	}

	return p[path.Len():], nil
}
