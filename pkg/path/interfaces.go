package path

type Marker interface {
	Equals(Marker) bool
}

type Indexable interface {
	Index() int
}

type Nameable interface {
	Name() string
}

type Index int

func (i Index) Equals(target Marker) bool {
	t, ok := target.(Index)
	if !ok {
		return false
	}

	return int(i) == int(t)
}

func (i Index) Index() int {
	return int(i)
}

type Name string

func (n Name) Equals(target Marker) bool {
	t, ok := target.(Name)
	if !ok {
		return false
	}

	return string(n) == string(t)
}

func (n Name) Name() string {
	return string(n)
}
