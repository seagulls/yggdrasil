package action

import (
	"gitlab.com/seagulls/yggdrasil/pkg/node"
)

// Set actions change the value of an attribute of a Node.  If the
// attribute does not yet exist then the attribute is created.
type Set struct {
	Attr  string      `json:"attr"`
	Value interface{} `json:"value"`

	target   node.Node
	original interface{}
}

func (s *Set) Apply(n node.Node) error {
	if o, ok := n.Attr(s.Attr); ok {
		s.original = o
	}

	s.target = n
	n.SetAttr(s.Attr, s.Value)

	return nil
}

func (s *Set) Rollback() {
	if s.original == nil {
		s.target.RemoveAttr(s.Attr)
		return
	}

	s.target.SetAttr(s.Attr, s.original)
}
