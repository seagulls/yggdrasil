package action

import (
	"gitlab.com/seagulls/yggdrasil/pkg/node"
)

// Create actions instantiate a new Node and assigns it as a child of
// the Node it is applied to.
type Create struct {
	Key string `json:"key"`

	target  node.Node
	created node.Node
}

func (c *Create) Apply(n node.Node) error {
	c.target = n
	child := node.NewMappedNode(c.Key)
	err := n.SetChild(child)
	if err == nil {
		c.created = child
	}

	return err
}

func (c *Create) Rollback() {
	if c.created != nil {
		c.target.RemoveChild(c.created.Key())
	}
}
