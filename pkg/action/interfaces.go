package action

import (
	"encoding/json"

	"gitlab.com/seagulls/yggdrasil/pkg/node"
)

// Action represents an action to be carried out against a target Node.  The
// action implementation must be agnostic of the Node context and provide for
// the action to be reversed should it be required.
type Action interface {
	Apply(node.Node) error
	Rollback()
}

type Marshaller interface {
	Register(Action) error
	Marshal(Action) (json.RawMessage, error)
	Unmarshal(json.RawMessage) (Action, error)
}
