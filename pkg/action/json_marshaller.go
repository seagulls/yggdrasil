package action

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
)

// jsonAction is a JSON encodable container for the Action interface
type jsonAction struct {
	Type string          `json:"type"`
	Data json.RawMessage `json:"data"`
}

// jsonMarshaller provides a two-way mapping between types implementing
// the Action interface and string representations thereof
type jsonMarshaller struct {
	d map[string]reflect.Type
	e map[reflect.Type]string
}

func NewJSONMarshaller() Marshaller {
	return &jsonMarshaller{
		d: make(map[string]reflect.Type),
		e: make(map[reflect.Type]string),
	}
}

func (j jsonMarshaller) encodeAction(act Action) (string, bool) {
	v, ok := j.e[reflect.TypeOf(act)]
	return v, ok
}

func (j jsonMarshaller) decodeType(act jsonAction) (Action, bool) {
	v, ok := j.d[act.Type]
	if !ok {
		return nil, false
	}

	// v is the type of the underlying struct rather than the type of the interface as
	// set in j.Register.  This means when New instantiates the struct, the pointer it
	// returns is the correct type implementing the Action interface.
	return reflect.New(v).Interface().(Action), true
}

func (j jsonMarshaller) Register(example Action) error {
	// get the reflected type of example
	et := reflect.TypeOf(example)
	// convert from `*action.Type` to `type`
	name := et.String()
	if idx := strings.LastIndex(name, `.`); idx >= 0 {
		name = name[idx+1:]
	}
	name = strings.ToLower(name)

	if v, ok := j.d[name]; ok {
		return fmt.Errorf(`type %q already registered as %q`, name, v.String())
	}

	// we need the element of the type here to avoid creating j pointer to j pointer in j.decodeType
	j.d[name] = et.Elem()
	j.e[et] = name

	return nil
}

func (j jsonMarshaller) Marshal(a Action) (json.RawMessage, error) {
	encoded, err := json.Marshal(a)
	if err != nil {
		return nil, err
	}

	t, ok := j.encodeAction(a)
	if !ok {
		return nil, fmt.Errorf("unknown action type %q", reflect.TypeOf(a))
	}

	v := jsonAction{
		Type: t,
		Data: encoded,
	}

	return json.Marshal(v)
}

func (j jsonMarshaller) Unmarshal(raw json.RawMessage) (Action, error) {
	var v jsonAction
	err := json.Unmarshal(raw, &v)

	var decoded Action
	decoded, ok := j.decodeType(v)
	if !ok {
		return nil, fmt.Errorf("unknown action type %q", v.Type)
	}

	err = json.Unmarshal(v.Data, decoded)
	return decoded, err
}
